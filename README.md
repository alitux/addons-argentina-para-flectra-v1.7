# Localización Argentina con soporte AFIP para Flectra V1.X

Localización Argentina lista para funcionar en Flectra V1.X. 

## Preinstalación

### Dependencias Sistema
- git
- gcc
- swig
- python-m2crypto

### Instalar dependencias Python
pip install -r requeriments.txt

## Instalación

- Clonar el repositorio.
- Declarar las carpetas "argentina" "dependencias" y "oca" en flectra.conf para que encuentre los addons
