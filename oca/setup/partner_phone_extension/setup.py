import setuptools

setuptools.setup(
    setup_requires=['setuptools-flectra'],
    flectra_addon=True,
)
