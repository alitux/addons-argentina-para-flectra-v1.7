##############################################################################
# For copyright and license notices, see __manifest__.py file in module root
# directory
##############################################################################
from flectra import models, fields


class ResPartner(models.Model):
    _inherit = 'res.partner'

    active = fields.Boolean(track_visibility='onchange')
